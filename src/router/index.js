import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/login.vue'
import home from '@/components/home.vue'
import admin from '@/views/administador/admins.vue'
import addAdmin from '@/views/administador/addAdmin.vue'
import editAdmin from '@/views/administador/editarAdmin.vue'
import adminInfo from '@/views/administador/adminInfo.vue'
import crearEquipos from '@/views/equipos/crearEquipo.vue'
import equiposInfo from '@/views/equipos/equiposInfo.vue'
import editarEquipo from '@/views/equipos/editarEquipos.vue'
import tarjetaDeportes from '@/views/configuraciones/tarjetaDeportes.vue'
import ejemplo from '@/components/ejemploError.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: login
  },
  {
    path: '/ejemplo',
    name: 'ejemplo',
    component: ejemplo
  },
  {
    path:'/home',
    name:'Equipos CX',
    component: home
  },
  {
    path: '/administradores',
    name:'Administradores CX',
    component:admin
  },
  {
    path: '/crear-admin',
    name:'Crear Administrador',
    component:addAdmin
  },
  {
    path: '/admin-info/:id',
    name:'Administrador info',
    component:adminInfo
  },
  {
    path: '/editar-admin/:id',
    name:'Editar administrador',
    component:editAdmin
  },
  {
    path: '/Crear-equipo',
    name:'crear equipo',
    component:crearEquipos
  },
  {
    path: '/equipos-info/:id',
    name:'Equipos info',
    component:equiposInfo
  },
  {
    path:'/editar-equipo/:id',
    name:'Editar equipo',
    component:editarEquipo

  },
  {
    path:'/configuracion',
    name:'Configuraciones',
    component: tarjetaDeportes
  },
  
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
