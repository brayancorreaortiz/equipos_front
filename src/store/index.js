import Vue from 'vue'
import Vuex from 'vuex'
import '@/slim/slim/slim.vue';
import slimCropper from '@/slim/slim/slim.vue';
Vue.component('slimCropper', slimCropper);
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
